import UIKit



func replaceVowels(phrase: String, replacementTag: Character) -> String {
    
    // make sure everything is lower case
    let newPhrase = phrase.lowercased()
    
    // Keep Track of vowels
    let vowels: [Character] = ["a","e","i","o","u", "y"]

    // Map to each character and replace if match
    let results = String(newPhrase.map {
        return vowels.contains($0) ? replacementTag : $0
     })
    
    return results
}

func removeSmallest(array: Array<Int>) -> Array<Int> {
    
    // make sure everything is lower case
    var resultArray = array
    
    var index = 0
    
    var alreadyPresent = false
    
    var currentLowest = resultArray.min()
    
    
    if resultArray.isEmpty {
        return resultArray
    } else {
        for number in resultArray {
            if(currentLowest == number && alreadyPresent){
            }else if (currentLowest == number){
                resultArray.remove(at: index)
                alreadyPresent = true
            }
            index += 1
        }
    }
    return resultArray
}


func splitCode(code: String) -> Array<String>{
    
    var resultArray: [String] = []
//
//    var str: [String] = []
//    var digit: [Int] = []
//
//    let zero: Unicode.Scalar = "0"
//    let nine: Unicode.Scalar = "9"
//
//    for letter in code.unicodeScalars {
//        switch letter.value {
//        case zero.value...nine.value:
//            digit.append(letter)
//        default:
//            str.append(letter)
//        }
//    }
    
    return resultArray
}




// Function calls

//Replace Vowels
//print("Replace Vowels: ")
//print(replaceVowels(phrase: "the aardvark", replacementTag: "#"))
//print(replaceVowels(phrase: "shakespeare", replacementTag: "*"))


// Remove smallest
//print("Remove Smallest: ")
//let lowestOne = [1, 2, 3, 4, 1, 5]
//print(removeSmallest(array: lowestOne))



print(splitCode(code: "TEWA8392"))


